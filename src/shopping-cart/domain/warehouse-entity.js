export default class WarehouseEntity {
  constructor(warehouseInput) {
    this.count = warehouseInput.count;
    this.product = warehouseInput.product;
  }
}
