export default class UserEntity {
  constructor(user) {
    this.fullName = user.fullName;
    this.address = user.address;
    this.city = user.city;
  }
}
