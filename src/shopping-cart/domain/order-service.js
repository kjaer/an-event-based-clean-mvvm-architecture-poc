export default class OrderService {
  constructor(orderRepository) {
    this.provider1 = "MOCK-PAYMENT-PROVIDER-1";
    this.orderRepository = orderRepository;
  }

  confirmPayment(paymentInfo) {
    const paymentInstrument = {
      owner: paymentInfo.fullName,
      number: paymentInfo.cardNumber,
      expiration: paymentInfo.expiryDate,
    };

    if (paymentInfo.vendor === this.provider1) {
      return this.orderRepository.confirmPaymentForProvider1(
        paymentInstrument,
        paymentInfo.amount
      );
    }
  }

  createOrder(orderPayload) {
    return this.confirmPayment(orderPayload);
  }
}
