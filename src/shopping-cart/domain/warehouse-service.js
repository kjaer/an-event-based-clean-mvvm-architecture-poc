export default class WarehouseService {
  constructor(warehouseRepository) {
    this.warehouseRepository = warehouseRepository;
  }

  retrieveProduct(product) {
    return this.warehouseRepository.retrieveProduct(product);
  }

  fetchInventory() {
    return this.warehouseRepository.fetchInventory();
  }

  // Auxiliary
  persistAvailableProducts(inventory) {
    this.warehouseRepository.persistAvailableProducts(inventory);
  }

  getAvailableProducts() {
    return this.warehouseRepository.getAvailableProducts();
  }

  cleanProducts() {
    this.warehouseRepository.clearStorage();
  }
}
