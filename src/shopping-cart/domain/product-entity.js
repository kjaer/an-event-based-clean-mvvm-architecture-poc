export default class ProductEntity {
  constructor(product) {
    this.SKU = product.sku;
    this.name = product.name;
    this.price = product.price;
    this.category = product.category;
    this.image = product.image;
  }
}
