export default class WarehouseRepository {
  constructor() {
    this.warehouseEndpoint =
      "https://run.mocky.io/v3/9263a4be-d7b1-405e-a31a-faa3a41600fc";
    this.warehouseName = "warehouse-inventory";
  }

  fetchInventory() {
    return fetch(this.warehouseEndpoint).then((result) => result.json());
  }

  retrieveProduct(product) {
    const rawInventory = globalThis.localStorage.getItem(this.warehouseName);
    const inventory = JSON.parse(rawInventory);
    return inventory.find(
      (inventoryItem) => inventoryItem.product.SKU === product.SKU
    );
  }

  // Auxiliary
  persistAvailableProducts(inventory) {
    globalThis.localStorage.setItem(
      this.warehouseName,
      JSON.stringify(inventory)
    );
  }

  getAvailableProducts() {
    const inventory = globalThis.localStorage.getItem(this.warehouseName);
    return JSON.parse(inventory);
  }

  clearStorage() {
    globalThis.localStorage.clear();
  }
}
