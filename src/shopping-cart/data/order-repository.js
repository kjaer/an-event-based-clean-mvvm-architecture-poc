export default class OrderRepository {
  confirmPaymentForProvider1(paymentInstrument, amount) {
    console.log(`${amount} has been charged!`);

    const response = {
      data: {
        correlationID: Math.floor(Math.random() * 5000) + 1,
        status: "OK",
        confirmed: true,
        vendor: "MOCK-PROVIDER-1",
      },
    };

    return Promise.resolve(response);
  }
}
