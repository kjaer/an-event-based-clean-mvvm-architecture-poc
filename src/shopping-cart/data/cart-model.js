import EventEmitter3 from "eventemitter3";

import ProductEntity from "../domain/product-entity";
import UserEntity from "../domain/user-entity";
import WarehouseEntity from "../domain/warehouse-entity";
import WarehouseService from "../domain/warehouse-service";
import WarehouseRepository from "./warehouse-repository";
import OrderService from "../domain/order-service";
import OrderRepository from "./order-repository";

class CartModel extends EventEmitter3 {
  constructor() {
    super();

    this.cart = [];
    this.wareHouseRepository = new WarehouseRepository();
    this.wareHouse = new WarehouseService(this.wareHouseRepository);

    this.orderRepository = new OrderRepository();
    this.orderService = new OrderService(this.orderRepository);

    this.eventType = {
      FETCH_INVENTORY: "fetch-inventory",
      ADDED_TO_CART: "added-to-cart",
      INVENTORY_UPDATED: "inventory-updated",
      REMOVED_FROM_CART: "removed-from-cart",
    };
  }

  fetchProductsFromWarehouse() {
    this.wareHouse
      .fetchInventory()
      .then(({ inventory }) =>
        inventory.map((item) => {
          const { count, ...warehouseItem } = item;
          const product = new ProductEntity(warehouseItem);

          return new WarehouseEntity({ count, product });
        })
      )
      .then((mappedWarehouseInventory) => {
        this.wareHouse.persistAvailableProducts(mappedWarehouseInventory);
        this.emit(this.eventType.FETCH_INVENTORY, mappedWarehouseInventory);
      });
  }

  addToCart(product) {
    const itemFromWarehouse = this.wareHouse.retrieveProduct(product);

    if (itemFromWarehouse.count > 0) {
      this.cart.push(itemFromWarehouse.product);
      this.emit(this.eventType.ADDED_TO_CART, this.cart);

      itemFromWarehouse.count -= 1;
      const itemWithReducedStock = new WarehouseEntity(itemFromWarehouse);
      this.updateInventory(itemWithReducedStock);
    }
  }

  removeFromCart(product) {
    const newCart = this.cart.filter(
      (productInCart) => productInCart.SKU !== product.SKU
    );
    const returningProducts = this.cart.filter(
      (productInCart) => productInCart.SKU === product.SKU
    );
    this.cart = newCart;
    this.emit(this.eventType.REMOVED_FROM_CART, this.cart);

    const itemFromWarehouse = this.wareHouse.retrieveProduct(product);
    itemFromWarehouse.count += returningProducts.length;
    const itemWithIncreasedStock = new WarehouseEntity(itemFromWarehouse);
    this.updateInventory(itemWithIncreasedStock);
  }

  placeOrder(orderInformation, amount, vendor) {
    const user = new UserEntity({
      fullName: orderInformation.fullName,
      address: orderInformation.address,
      city: orderInformation.city,
    });

    const paymentInstrument = {
      cardNumber: orderInformation.cardNumber,
      expiryDate: orderInformation.expiryDate,
      vendor,
    };

    return this.orderService.createOrder({
      amount,
      ...user,
      ...paymentInstrument,
    });
  }

  // Auxiliary
  updateInventory(item) {
    const currentInventory = this.wareHouse.getAvailableProducts();
    const newInventory = currentInventory.map((inventoryItem) =>
      inventoryItem.product.SKU === item.product.SKU ? item : inventoryItem
    );

    this.emit(this.eventType.INVENTORY_UPDATED, newInventory);
    this.wareHouse.persistAvailableProducts(newInventory);
  }

  getAvailableProducts() {
    const currentInventory = this.wareHouse.getAvailableProducts();

    if (currentInventory) {
      this.emit(this.eventType.FETCH_INVENTORY, currentInventory);
      return true;
    }

    return false;
  }

  clearProductsOnExit() {
    this.wareHouse.cleanProducts();
  }
}

export default new CartModel();
