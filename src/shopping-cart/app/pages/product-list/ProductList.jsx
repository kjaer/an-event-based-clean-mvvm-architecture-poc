import React, { useState, useEffect } from "react";

import ProductListViewmodel from "./product-list-viewmodel";
import Product from "../../components/product/Product";
import "./product-list-style.css";

function ProductList() {
  const [vm] = useState(() => new ProductListViewmodel());
  const [inventory, setInventory] = useState([]);

  useEffect(() => {
    vm.on(vm.eventType.PRODUCTS_RECEIVED, (inventory) => {
      setInventory(inventory);
    });
    vm.fetchAvailableProducts();
    return () => {
      vm.off(vm.eventType.PRODUCTS_RECEIVED);
    };
  }, []);

  function addToCart(item) {
    vm.addToCart(item);
  }

  return (
    <>
      <h1>Product List</h1>

      <section className="product-container">
        {inventory.map((item) => (
          <Product key={item.product.SKU} item={item} addToCart={addToCart} />
        ))}
      </section>
    </>
  );
}

export default ProductList;
