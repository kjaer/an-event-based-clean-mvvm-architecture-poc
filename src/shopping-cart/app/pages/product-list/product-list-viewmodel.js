import EventEmitter3 from "eventemitter3";
import cartModel from "../../../data/cart-model";

export default class ProductListViewmodel extends EventEmitter3 {
  constructor() {
    super();
    this.cartModel = cartModel;

    this.cartModel.on(this.cartModel.eventType.FETCH_INVENTORY, (inventory) => {
      this.onProductReceived(inventory);
    });

    this.cartModel.on(
      this.cartModel.eventType.INVENTORY_UPDATED,
      (newInventory) => {
        this.onProductReceived(newInventory);
      }
    );

    this.eventType = {
      PRODUCTS_RECEIVED: "products-received",
    };
  }

  onProductReceived(inventory) {
    this.emit(this.eventType.PRODUCTS_RECEIVED, inventory);
  }

  fetchAvailableProducts() {
    const isInventoryAvailable = this.cartModel.getAvailableProducts();

    if (!isInventoryAvailable) {
      this.cartModel.fetchProductsFromWarehouse();
    }
  }

  addToCart(item) {
    this.cartModel.addToCart(item.product);
  }
}
