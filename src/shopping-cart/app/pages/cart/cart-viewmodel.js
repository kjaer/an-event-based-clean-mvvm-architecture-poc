import EventEmitter3 from "eventemitter3";
import cartModel from "../../../data/cart-model";

export default class CartViewModel extends EventEmitter3 {
  constructor() {
    super();
    this.cartModel = cartModel;

    this.cartModel.on(this.cartModel.eventType.REMOVED_FROM_CART, () => {
      const newCart = this.getProductsInCart();
      this.emit(this.eventType.REMOVED, newCart);
    });

    this.eventType = {
      REMOVED: "products-removed",
    };
  }

  getProductsInCart() {
    const uniqueItems = this.cartModel.cart.reduce((uniqueCart, product) => {
      const existingItem = uniqueCart.find(
        (uniqueItem) => uniqueItem.product.SKU === product.SKU
      );

      if (existingItem) {
        existingItem.purchaseCount += 1;
        existingItem.total = Math.ceil(
          existingItem.purchaseCount * existingItem.product.price
        );
        return uniqueCart;
      }

      uniqueCart.push({ purchaseCount: 1, total: product.price, product });
      return uniqueCart;
    }, []);

    return uniqueItems;
  }

  removeProductInCart(item) {
    this.cartModel.removeFromCart(item.product);
  }
}
