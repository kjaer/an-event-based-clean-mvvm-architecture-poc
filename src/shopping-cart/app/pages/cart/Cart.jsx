import React, { useState, useEffect } from "react";

import { Link } from "react-router-dom";
import removeCartIcon from "../../../../../public/assets/noun_Remove_from_Cart.svg";

import CartViewModel from "./cart-viewmodel";

function Cart() {
  const [vm] = useState(() => new CartViewModel());
  const [items, setItems] = useState([]);

  useEffect(() => {
    vm.on(vm.eventType.REMOVED, (items) => {
      setItems(items);
    });
    const items = vm.getProductsInCart();

    setItems(items);
  }, []);

  function removeProduct(product) {
    vm.removeProductInCart(product);
  }

  return (
    <>
      <h1>Cart</h1>
      <section>
        <table>
          <thead>
            <tr>
              <th>Product Name</th>
              <th>Product Category</th>
              <th>Product Price</th>
              <th>Count</th>
              <th>Total</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            {items.map((item) => (
              <tr key={item.product.SKU}>
                <td>{item.product.name}</td>
                <td>{item.product.category}</td>
                <td>{item.product.price}</td>
                <td>{item.purchaseCount}</td>
                <td>{item.total}</td>
                <td>
                  <a href="#" onClick={() => removeProduct(item)}>
                    <em>
                      <img
                        src={removeCartIcon}
                        alt="Add to Cart"
                        height="25"
                        align="top"
                      />{" "}
                      &nbsp; Remove
                    </em>
                  </a>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </section>
      <div style={{ textAlign: "right" }}>
        <hr />
        <Link to="/checkout">
          <b>Proceed to checkout ➜</b>
        </Link>
      </div>
    </>
  );
}

export default Cart;
