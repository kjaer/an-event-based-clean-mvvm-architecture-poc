import React, { useState, useEffect } from "react";
import CheckoutViewModel from "./checkout-viewmodel";
import "./checkout-style.css";

function Checkout() {
  const [vm] = useState(() => new CheckoutViewModel());
  const [items, setItems] = useState([]);
  const [total, setTotal] = useState(0);

  useEffect(() => {
    const { items, totalAmount } = vm.getCartSummary();
    setItems(items);
    setTotal(totalAmount);
  }, []);
  /**/
  const [userForm, setUserForm] = useState({
    fullName: "",
    address: "",
    city: "",
    cardNumber: "",
    expiryDate: "",
  });

  function onUserFormChange(data, key) {
    setUserForm((prevState) => ({
      ...prevState,
      [key]: data,
    }));
  }

  function placeOrder() {
    vm.placeOrder(userForm, total);
  }

  return (
    <>
      <h1>Checkout</h1>
      <section>
        <header>
          <h2>
            Please provide payment and shipping information for your delivery.
          </h2>
        </header>
        <form>
          <label htmlFor="fullname">Full Name:</label>
          <input
            type="text"
            id="fullname"
            placeholder="e.g. John Doe"
            size={90}
            onChange={(e) => onUserFormChange(e.target.value, "fullName")}
          />

          <fieldset>
            <label htmlFor="cardnumber">Card Number:</label>
            <input
              type="text"
              id="cardnumber"
              placeholder="xxxx xxxx xxxx xxxx"
              size={87}
              onChange={(e) => onUserFormChange(e.target.value, "cardNumber")}
            />

            <label htmlFor="expiry-date">Exp. Date</label>
            <input
              type="text"
              id="expiry-date"
              placeholder="xx/xx"
              size={87}
              onChange={(e) => onUserFormChange(e.target.value, "expiryDate")}
            />
          </fieldset>

          <br />
          <label htmlFor="shipping-address">Where to deliver?</label>
          <textarea
            cols="90"
            rows="5"
            id="delivery-address"
            placeholder="Street name, building number, postal code"
            onChange={(e) => onUserFormChange(e.target.value, "address")}
          />
          <input
            type="text"
            id="delivery-city"
            placeholder="City"
            size={90}
            onChange={(e) => onUserFormChange(e.target.value, "city")}
          />

          <button type="button" onClick={placeOrder}>
            Place Order
          </button>
        </form>

        <hr className="divider" />

        <header>
          <h2>Products</h2>
        </header>
        <table>
          <thead>
            <tr>
              <th>Product Name</th>
              <th>Product Category</th>
              <th>Product Price</th>
              <th>Count</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            {items.map((item) => (
              <tr key={item.product.SKU}>
                <td>{item.product.name}</td>
                <td>{item.product.category}</td>
                <td>{item.product.price}</td>
                <td>{item.purchaseCount}</td>
                <td>{item.total}</td>
              </tr>
            ))}
          </tbody>
          <tfoot>
            <tr>
              <td colSpan={4}>Total:</td>
              <td>{total}</td>
            </tr>
          </tfoot>
        </table>
      </section>
    </>
  );
}

export default Checkout;
