import EventEmitter3 from "eventemitter3";
import cartModel from "../../../data/cart-model";

export default class CheckoutViewModel extends EventEmitter3 {
  constructor() {
    super();
    this.cartModel = cartModel;
  }

  getCartSummary() {
    const uniqueItems = this.cartModel.cart.reduce((uniqueCart, product) => {
      const existingItem = uniqueCart.find(
        (uniqueItem) => uniqueItem.product.SKU === product.SKU
      );

      if (existingItem) {
        existingItem.purchaseCount += 1;
        existingItem.total = Math.ceil(
          existingItem.purchaseCount * existingItem.product.price
        );
        return uniqueCart;
      }

      uniqueCart.push({ purchaseCount: 1, total: product.price, product });
      return uniqueCart;
    }, []);

    const totalAmount = uniqueItems.reduce((sum, { total }) => sum + total, 0);

    return {
      items: uniqueItems,
      totalAmount,
    };
  }

  placeOrder(orderInformation, amount) {
    const PAYMENT_VENDOR = "MOCK-PAYMENT-PROVIDER-1";

    this.cartModel
      .placeOrder(orderInformation, amount, PAYMENT_VENDOR)
      .then((result) => {
        const { data: paymentConfirmation } = result;
        if (!paymentConfirmation.confirmed) {
          alert("Payment Failed!");
          return null;
        }

        /** * */
        /* Do Stuff with shipment etc. */
        /** * */

        alert("Order Created!");
      });
  }
}
