import React, { useState, useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import RootViewModel from "./root-viewmodel";

import ProductList from "./pages/product-list/ProductList";
import Cart from "./pages/cart/Cart";
import Checkout from "./pages/checkout/Checkout";

import Navigation from "./components/navigation/Navigation";

function App() {
  const [vm] = useState(() => new RootViewModel());

  useEffect(() => {
    globalThis.addEventListener("beforeunload", () => {
      vm.flushProductsData();
    });
  }, []);

  return (
    <>
      <BrowserRouter>
        <Navigation />

        <main style={{ paddingTop: 0 }}>
          <Routes>
            <Route exact path="/" element={<ProductList />} />
            <Route path="/cart" element={<Cart />} />
            <Route path="/checkout" element={<Checkout />} />
          </Routes>
        </main>

        <footer>
          <hr />
          <p>
            Crate.io - Crate Solutions Team / Frontend Architecture PoC with
            clean and MVVM Architecture
          </p>
        </footer>
      </BrowserRouter>
    </>
  );
}

export default App;
