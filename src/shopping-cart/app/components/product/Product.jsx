import React from "react";

import addCartIcon from "../../../../../public/assets/noun_Add_to_Cart.svg";

function Product(props) {
  const {
    item,
    item: { count, product },
  } = props;
  return (
    <aside>
      <img alt="HTML only" src={product.image} />
      <h3>{product.name}</h3>
      <p>€ {product.price}</p>
      <p>
        <button onClick={() => props.addToCart(item)} disabled={count === 0}>
          <img src={addCartIcon} alt="Add to Cart" height="25" align="top" />
          &nbsp;Add to Cart
        </button>
      </p>
      <p>
        <small>
          availability: <b>{count}</b>
        </small>
      </p>
    </aside>
  );
}

export default Product;
