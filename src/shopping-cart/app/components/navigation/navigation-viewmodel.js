import EventEmitter3 from "eventemitter3";
import cartModel from "../../../data/cart-model";

export default class NavigationViewModel extends EventEmitter3 {
  constructor() {
    super();

    this.cartModel = cartModel;
    this.cartModel.on(this.cartModel.eventType.ADDED_TO_CART, (cart) => {
      this.onUpdateProductInCart(cart);
    });
    this.cartModel.on(this.cartModel.eventType.REMOVED_FROM_CART, (cart) => {
      this.onUpdateProductInCart(cart);
    });

    this.eventType = {
      CART_UPDATED: "cart-updated",
    };
  }

  onUpdateProductInCart(cart) {
    const uniqueProducts = cart
      .map((product) => product.SKU)
      .filter((sku, idx, skuList) => skuList.indexOf(sku) === idx);

    this.emit(this.eventType.CART_UPDATED, uniqueProducts.length);
  }
}
