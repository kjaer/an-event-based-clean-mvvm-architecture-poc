import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import NavigationViewModel from "./navigation-viewmodel";

import storeIcon from "../../../../../public/assets/noun_Commerce.svg";
import cartIcon from "../../../../../public/assets/noun_Shopping_Cart.svg";
import "./navigation-style.css";

function Navigation() {
  const [vm] = useState(() => new NavigationViewModel());
  const [cartCount, setCartCount] = useState(0);

  useEffect(() => {
    vm.on(vm.eventType.CART_UPDATED, (newCount) => {
      setCartCount(newCount);
    });
    return () => {
      vm.off(vm.eventType.CART_UPDATED);
    };
  }, []);

  return (
    <header>
      <nav className="navigation">
        <Link to="/">
          <img alt="Logo" src={storeIcon} className="logo" />
          Plugin Apps PoC
        </Link>
        <ul>
          <li>
            {" "}
            <Link to="/">Product List</Link>
          </li>
          <li>
            <Link to="/cart">
              <img alt="cart" src={cartIcon} className="cart" />({cartCount})
            </Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}

export default Navigation;
