import EventEmitter3 from "eventemitter3";
import cartModel from "../data/cart-model";

export default class RootViewModel extends EventEmitter3 {
  constructor() {
    super();
    this.cartModel = cartModel;
  }

  flushProductsData() {
    this.cartModel.clearProductsOnExit();
  }
}
