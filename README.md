# An event based, blend-in Clean Architecture and MVVM Architecture PoC

This is the PoC implementation of proposed architecture for the plugin application.
Here is the Business for this app:

#### Shopping Cart

Topic was influced from the blogpost of [Yuri Khomyakov](https://yuriktech.com/2019/06/11/Implementing-Clean-Architecture/)

> lets build something overused, like a shopping cart.

Simply, you can add/remove the products into shopping cart.
Before you add the products, app checking warehouse inventory if there is enough
amount of product available, then let you add the product.

When you attempt to remove the product, It's not only removing from your shopping cart,
but also returns them back to warehouse inventory.

These two business expectation enough to illustrate new architecture implementation.

### How to run this Web App

This repository is uses yarn as a package manager. After you clone, simply run

```shell
yarn install
```

then

```shell
yarn start
```

project served over port `16988`. Head over your browser and open
`http://0.0.0.0:16988`


There are 2 additional commands:

```shell
yarn format
```

for using prettier to format, and

```shell
yarn build
```

for preparing source for bundling.

---

This repository created courtesy of Robin Wieruch's [React with Webpack Tutorial](https://github.com/rwieruch/minimal-react-webpack-babel-setup). work.

- [React with Webpack Tutorial](https://www.robinwieruch.de/minimal-react-webpack-babel-setup/).

Read how to set it up yourself: [React with Webpack Tutorial](https://www.robinwieruch.de/minimal-react-webpack-babel-setup/).


## Features

- React 16
- Webpack 5
- Babel 7
