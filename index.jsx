import React from "react";
import ReactDOM from "react-dom";

import App from "./src/shopping-cart/app/Root";
import "mvp.css";

ReactDOM.render(<App />, document.getElementById("app"));

module.hot.accept();
